# Tic-Tac-Toe

A *React-Native* tic-tac-toe app with an unbeatable computer opponent. Implemented in *ClojureScript* with [re-natal](https://github.com/drapanjanas/re-natal).

![demo](/doc/images/demo.gif)
