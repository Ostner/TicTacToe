(ns tic-tac-toe.subs
  (:require [re-frame.core :refer [reg-sub]]
            [tic-tac-toe.model :refer [result]]))

(reg-sub
 :get-field
 (fn [db [_ square]]
   (db :field)))

(reg-sub
 :nav-state
 (fn [db _]
   (db :nav-state)))

(reg-sub
 :get-turn
 (fn [db _]
   (db :turn)))

(reg-sub
 :get-human
 (fn [db _]
   (db :human)))

(reg-sub
 :result
 :<- [:get-human]
 :<- [:get-field]
 (fn [[human field] _]
   (if-let [winner (result field)]
     (cond
       (= winner :draw) "Draw"
       (= winner human) "You win"
       :else "You lose"))))
