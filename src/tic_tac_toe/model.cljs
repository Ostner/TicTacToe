(ns tic-tac-toe.model)

(declare maximize)
(declare minimize)

(def ^:const lower -100)
(def ^:const upper 100)
(def ^:const directions [;; rows
                         [[0 0] [0 1] [0 2]]
                         [[1 0] [1 1] [1 2]]
                         [[2 0] [2 1] [2 2]]
                         ;; columns
                         [[0 0] [1 0] [2 0]]
                         [[0 1] [1 1] [2 1]]
                         [[0 2] [1 2] [2 2]]
                         ;;diagonals
                         [[0 0] [1 1] [2 2]]
                         [[2 0] [1 1] [0 2]]])


(defn minimax [{:keys [turn field]}]
  (first (maximize {:turn turn :field field} lower upper)))

(declare no-moves?)

(defn result [field]
  (let [dir-vals (map (fn [dir]
                        (map #(field %) dir))
                      directions)
        winner (reduce (fn [prev-dir curr-dir]
                         (if-let [res (reduce (fn [prev curr]
                                                (if (= curr prev)
                                                  curr
                                                  (reduced nil)))
                                              curr-dir)]
                           (reduced res)
                           nil))
                       nil
                       dir-vals)]
    (cond
      (boolean winner) winner
      (no-moves? field) :draw
      :else nil)))


;;; Private helper

(defn- actions
  [field]
  (for [[coord sym] field :when (nil? sym)]
    coord))

(defn- switch-turn
  [turn]
  (. js/Math abs (dec turn)))

(defn- transition
  [{:keys [turn field]} action]
  {:turn (switch-turn turn)
   :field (assoc field action turn)})

(defn- no-moves? [field]
  (every? (complement nil?) (vals field)))

(defn- winner? [field]
  (let [dir-vals (mapv (fn [dir]
                         (mapv #(field %) dir))
                       directions)
        dir-same-val? (map (fn [dir]
                             (if-let [el (first dir)]
                               (every? #(= % el) dir)))
                           dir-vals)]
    (boolean (some true? dir-same-val?))))

(defn- terminal? [field]
  (or (no-moves? field) (winner? field)))

(defn- eval [field]
  (if (winner? field)
    1
    0))

(defn- maximize
  [{:keys [turn field] :as state} alpha beta]
  (cond
    (terminal? field) [nil (- (eval field))]
    :else (->> (actions field)
               (map (fn [a] [a (transition state a)]))
               (reduce (fn [best curr]
                         (let [action (first curr)
                               state (second curr)
                               utility (second (minimize state (best :alpha) beta))
                               new-best (if (<= utility (best :utility))
                                          best
                                          {:alpha (max utility (best :alpha))
                                           :action action
                                           :utility utility})]
                           (if (>= utility beta)
                             (reduced new-best)
                             new-best)))
                       {:alpha lower :action nil :utility lower})
               ((fn [m] [(m :action) (m :utility)])))))

(defn- minimize
  [{:keys [turn field] :as state} alpha beta]
  (cond
    (terminal? field) [nil (eval field)]
    :else (->> (actions field)
               (map (fn [a] [a (transition state a)]))
               (reduce (fn [best curr]
                         (let [action (first curr)
                               state (second curr)
                               utility (second (maximize state alpha (best :beta)))
                               new-best (if (>= utility (best :utility))
                                          best
                                          {:beta (min utility (best :beta))
                                           :action action
                                           :utility utility})]
                           (if (<= utility alpha)
                             (reduced new-best)
                             new-best)))
                       {:beta upper :action nil :utility upper})
               ((fn [m] [(m :action) (m :utility)])))))
