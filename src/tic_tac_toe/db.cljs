(ns tic-tac-toe.db
  (:require [clojure.spec :as s]))

(s/def ::player #{0 1})
(s/def ::human ::player)
(s/def ::turn ::player)
(s/def ::square (s/tuple (s/and int? #(< % 3))
                         (s/and int? #(< % 3))))
(s/def ::field (s/map-of ::square (s/nilable ::player) :count 9 :distinct true))

(s/def ::index integer?)
(s/def ::key keyword?)
(s/def ::routeName keyword?)
(s/def ::route (s/keys :req-un [::key ::routeName]))
(s/def ::routes (s/coll-of ::route))
(s/def ::nav-state (s/keys :req-un [::index ::routes]))
(s/def ::app-db (s/keys :req-un [::human ::turn ::field ::nav-state]))

(def empty-field {[0 0] nil [0 1] nil [0 2] nil
                  [1 0] nil [1 1] nil [1 2] nil
                  [2 0] nil [2 1] nil [2 2] nil})

;; initial app-db
(def app-db {:human 0
             :turn 0
             :field empty-field
             :nav-state {:index 0
                         :routes [{:key :Home :routeName :Home}
                                  {:key :Game :routeName :Game}]}})
