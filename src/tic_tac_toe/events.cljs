(ns tic-tac-toe.events
  (:require
   [re-frame.core :refer [reg-event-db reg-event-fx reg-cofx inject-cofx after]]
   [clojure.spec :as s]
   [tic-tac-toe.db :as db :refer [app-db empty-field]]
   [tic-tac-toe.model :refer [minimax]]))

;; -- Interceptors ------------------------------------------------------------
;;
;; See https://github.com/Day8/re-frame/blob/master/docs/Interceptors.md
;;
(defn check-and-throw
  "Throw an exception if db doesn't have a valid spec."
  [spec db [event]]
  (when-not (s/valid? spec db)
    (let [explain-data (s/explain-data spec db)]
      (throw (ex-info (str "Spec check after " event " failed: " explain-data) explain-data)))))

(def validate-spec
  (if goog.DEBUG
    (after (partial check-and-throw ::db/app-db))
    []))

;; -- Helpers --------------------------------------------------------------

(defn- switch [turn]
  (case turn
    1 0
    0 1))

;; -- Handlers --------------------------------------------------------------


(reg-event-db
 :initialize-db
 validate-spec
 (fn [_ _]
   app-db))

(reg-event-db
 :reset-db
 validate-spec
 (fn [db _]
   (-> db
       (assoc :field empty-field)
       (assoc :turn 0)
       (assoc :human 0))))

(reg-event-db
 :make-ai-move
 validate-spec
 (fn [db _]
   (let [player (switch (db :turn))
         action (minimax {:turn player
                          :field (db :field)})]
     (assoc-in db [:field (first action)] player))))

(reg-cofx
 :minimax
 (fn [cofx _]
   (let [db (cofx :db)
         square (minimax {:turn (db :turn)
                          :field (db :field)})]
     (assoc cofx :ai-square square))))

(reg-event-fx
 :ai-move
 [validate-spec (inject-cofx :minimax)]
 (fn [cofx _]
   (let [square (cofx :ai-square)
         db (cofx :db)
         turn (db :turn)]
     {:db (-> db
              (update :turn switch)
              (assoc-in [:field square] turn))})))

(reg-event-fx
 :human-move
 validate-spec
 (fn [cofx [_ square]]
   (let [db (cofx :db)
         turn (db :turn)
         new-db (-> db
                    (update :turn switch)
                    (assoc-in [:field square] turn))
         moves-left? (some nil? (vals (:field new-db)))]
     (if moves-left?
       {:db new-db
        ;; Uses dispatch-later because order isn't guaranteed for effects map
        ;; and :make-ai-move is dependent on the new state of the app-db.
        :dispatch-later [{:ms 100
                          :dispatch [:ai-move]}]}
       {:db new-db}))))

(reg-event-db
 :nav-to-game
 validate-spec
 (fn [db _]
   (update-in db [:nav-state :index] inc)))

(reg-event-db
 :nav-to-home
 validate-spec
 (fn [db _]
   (update-in db [:nav-state :index] dec)))

(reg-event-fx
 :reset-game
 (fn [_ _]
   {:dispatch-n [[:reset-db] [:nav-to-home]]}))

(reg-event-db
 :set-human
 validate-spec
 (fn [db [_ player]]
   (assoc db :human player)))

(reg-event-fx
 :new-game
 (fn [cofx [_ human]]
   (let [move? (= human 1)
         events (filter (complement nil?)
                        (list [:nav-to-game]
                              [:set-human human]
                              (when move? [:ai-move])))]
     {:dispatch-n events})))

(reg-event-db
 :set-turn
 validate-spec
 (fn [db [_ turn]]
   (assoc db :turn turn)))
