(ns tic-tac-toe.ios.core
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [dispatch dispatch-sync subscribe]]
            [tic-tac-toe.events]
            [tic-tac-toe.subs]
            [tic-tac-toe.model]
            [tic-tac-toe.ios.game :refer [game-scene]]
            [tic-tac-toe.ios.home :refer [home-scene]]
            [tic-tac-toe.ios.ui :refer [app-registry stack-navigator add-nav-helpers]]))

(enable-console-print!)

(def stack-router {:Home {:screen (r/reactify-component home-scene)}
                   :Game {:screen (r/reactify-component game-scene)}})
(def navigator (r/adapt-react-class (stack-navigator (clj->js stack-router)
                                                     (clj->js {:mode :modal
                                                               :headerMode :none}))))

(defn app-root []
  (let [nav-state (subscribe [:nav-state])]
    [navigator {:navigation (add-nav-helpers
                             (clj->js {:state @nav-state
                                       :dispatch #(dispatch [:nav-to-home])}))}]))
(defn init []
  (dispatch-sync [:initialize-db])
  (.registerComponent app-registry "TicTacToe" #(r/reactify-component app-root)))
