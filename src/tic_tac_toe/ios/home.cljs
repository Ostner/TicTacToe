(ns tic-tac-toe.ios.home
  (:require [tic-tac-toe.ios.ui :refer [view button picker picker-item]]
            [tic-tac-toe.ios.styles :refer [styles font-color]]
            [re-frame.core :refer [dispatch subscribe]]
            [reagent.core :as r]))

(defn home-scene []
  (let [turn (r/atom 0)]
    (fn []
      [view {:style (styles :home-container)}
       [view {:style (styles :game-properties-container)}
        [picker {:style (styles :picker)
                 :on-value-change #(reset! turn %)
                 :selected-value @turn}
         [picker-item {:label "Player X" :value 0 :color font-color}]
         [picker-item {:label "Player O" :value 1 :color font-color}]]]
       [view {:style (styles :start-game-container)}
        [button {:color font-color
                 :title "Let's play"
                 :on-press #(dispatch [:new-game @turn])}]]])))
