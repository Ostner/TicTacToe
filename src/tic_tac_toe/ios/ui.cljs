(ns tic-tac-toe.ios.ui
  (:require [reagent.core :as r]))

(def ReactNative (js/require "react-native"))
(def app-registry (.-AppRegistry ReactNative))
(def text (r/adapt-react-class (.-Text ReactNative)))
(def view (r/adapt-react-class (.-View ReactNative)))
(def touchable-highlight (r/adapt-react-class (.-TouchableHighlight ReactNative)))
(def button (r/adapt-react-class (.-Button ReactNative)))
(def picker (r/adapt-react-class (.-Picker ReactNative)))
(def picker-item (r/adapt-react-class (.. ReactNative -Picker -Item)))

(def ReactNavigation (js/require "react-navigation"))
(def stack-navigator (.-StackNavigator ReactNavigation))
(def add-nav-helpers (.-addNavigationHelpers ReactNavigation))

(def font-awesome (js/require "react-native-vector-icons/FontAwesome"))
(def icon (r/adapt-react-class (.-default font-awesome)))
