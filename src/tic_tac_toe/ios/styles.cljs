(ns tic-tac-toe.ios.styles)

(def background-color "#3E4030")
(def font-color "#959a79")
(def grid-color "#707458")
(def square-color "#313615")

(def styles
  {:container {:flex 1
               :background-color background-color
               :justify-content :center
               :align-items :center}

   :home-container {:flex 1
                    :justify-content :center
                    :align-items :center
                    :background-color background-color}

   :start-game-container {:flex 1
                          :justify-content :center}

   :game-properties-container {:flex 3
                               :justify-content :center}

   :game-over-message-container {:flex 1
                                 :justify-content :center}

   :new-game-container {:flex 1
                        :justify-content :center}

   :grid {:width 355
          :height 355
          :padding 5
          :background-color background-color
          :border-width 1
          :border-color grid-color
          :justify-content :space-between}

   :row {:flex-direction :row
         :justify-content :space-between}

   :square {:width 110
            :height 110
            :background-color square-color
            :border-width 1
            :border-color grid-color
            :padding 20}

   :symbol {:text-align :center
            :font-size 70
            :color font-color}

   :game-over-label {:font-size 60
                     :color font-color}

   :picker {:width 180}})
