(ns tic-tac-toe.ios.game
  (:require [tic-tac-toe.ios.styles :refer [styles font-color]]
            [re-frame.core :refer [subscribe dispatch]]
            [tic-tac-toe.ios.ui :refer [view icon text touchable-highlight button]]))

(defn- player->symbol [player]
  (case player
    0 "times"
    1 "circle-o"
    "rocket"))

(defn- move-allowed? [square]
  (nil? square))

(defn game-scene []
  (let [field (subscribe [:get-field])
        result (subscribe [:result])]
    [view {:style (styles :container)}
     [view {:style (styles :game-over-message-container)}
      (when @result
        [text {:style (styles :game-over-label)} @result])]
     [view {:style (styles :grid)}
      (doall
       (for [row (range 3)]
         ^{:key row}
         [view {:style (styles :row)}
          (doall
           (for [col (range 3)]
             ^{:key col}
             [touchable-highlight
              {:on-press #(if (move-allowed? (@field [row col]))
                            (dispatch [:human-move [row col]]))}
              [view {:style (styles :square)}
               (when (@field [row col])
                 [icon {:style (styles :symbol)
                        :name (player->symbol (@field [row col]))}])]]))]))]
     [view {:style (styles :new-game-container)}
      (when @result
        [button {:color font-color
                 :title "New Game"
                 :on-press #(dispatch [:reset-game])}])]]))
